using ShipCapstone.ViewModels;

namespace ShipCapstone;

public sealed partial class MainPage : Page
{
    private MainViewModel vm;

    public MainPage()
    {
        this.InitializeComponent();
        vm = (MainViewModel)DataContext;
        vm.PlayGameStartSound();
    }

    private void Canvas_Keyboard(object sender, Microsoft.UI.Xaml.Input.KeyRoutedEventArgs e)
    {
        if (e.Key == Windows.System.VirtualKey.Right)
        {
            vm.MoveRightCommand.Execute(e);
        }
        else if (e.Key == Windows.System.VirtualKey.Up)
        {
            vm.MoveTopCommand.Execute(e);
        }
        else if (e.Key == Windows.System.VirtualKey.Left)
        {
            vm.MoveLeftCommand.Execute(e);
        }
        else if (e.Key == Windows.System.VirtualKey.Down)
        {
            vm.MoveBottomCommand.Execute(e);
        }
    }
}
