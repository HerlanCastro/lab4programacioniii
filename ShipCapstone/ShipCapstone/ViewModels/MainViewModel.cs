using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Media;
using Windows.Media.Core;
using Windows.Media.Playback;
using Windows.Storage;

namespace ShipCapstone.ViewModels;
internal partial class MainViewModel : ObservableObject
{
    private MediaPlayer mediaPlayer;

    public MainViewModel()
    {
        mediaPlayer = new MediaPlayer();
    }

    public void PlayGameStartSound()
    {
        try
        {
            var soundUri = new Uri("C:\\Users\\herlan\\Downloads\\hhhh\\ShipCapstone\\ShipCapstone\\ShipCapstone\\ShipCapstone\\Assets\\Alone.wav", UriKind.Absolute);
            mediaPlayer.Source = MediaSource.CreateFromUri(soundUri);
            mediaPlayer.Play();
        }
        catch (Exception ex)
        {
            System.Diagnostics.Debug.WriteLine($"Error al reproducir el sonido: {ex.Message}");
        }
    }

    [ObservableProperty]
    private int canvasTop = 450;

    [ObservableProperty]
    private int canvasLeft = 100;
    
    
    [RelayCommand]
    private void MoveLeft()
    {
        CanvasLeft -= 10;
    }
    [RelayCommand]
    private void MoveTop()
    {
        CanvasTop -= 10;
    }
    [RelayCommand]
    private void MoveRight()
    {
        CanvasLeft += 10;
    }
    [RelayCommand]
    private void MoveBottom()
    {
        CanvasTop += 10;
    }
   
}
